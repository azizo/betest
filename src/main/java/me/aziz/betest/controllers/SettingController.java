package me.aziz.betest.controllers;

import me.aziz.betest.models.Component;
import me.aziz.betest.models.ComponentDTO;
import me.aziz.betest.services.ComponentSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;


@Controller
@RequestMapping("/setting/components")
public class SettingController {
    @Autowired
    private ComponentSettingService service;

    @GetMapping()
    public String renderComponentSettingForm(Model model) {
        ComponentDTO componentDTO = new ComponentDTO();
        List<Component> componentList;

        componentList = service.getSetting();
        componentList.sort(Comparator.comparing(Component::getRanking));

        componentDTO.setComponents(componentList);

        model.addAttribute("form", componentDTO);
        return "component-setting-form";
    }

    @PostMapping("save")
    public String saveComponentSetting(@ModelAttribute ComponentDTO componentDTO) {
        // TODO: find a better way to handle duplicated date (DTO vs. Original object)
        //   service.deleteOldSetting();
        service.saveSetting(componentDTO.getComponents());
        return "redirect:/api/components";
    }

}
