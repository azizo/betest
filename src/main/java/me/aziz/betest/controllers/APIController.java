package me.aziz.betest.controllers;


import me.aziz.betest.models.Component;
import me.aziz.betest.models.ComponentDTO;
import me.aziz.betest.services.ComponentSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class APIController {

    @Autowired
    private ComponentSettingService service;

    @GetMapping("components")
    public List<Component> GetSetting() {
        return service.getSetting();
    }
}