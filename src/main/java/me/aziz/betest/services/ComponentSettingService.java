package me.aziz.betest.services;

import me.aziz.betest.models.Component;
import me.aziz.betest.models.ComponentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ComponentSettingService {

    @Autowired
    private ComponentRepo componentRepo;

    public List<Component> getSetting() {
        return componentRepo.findAll();
    }

    public void saveSetting(List<Component> components) {
        componentRepo.saveAll(components);
    }

    public List<Component> insertSampleData() {
        Component carousel = new Component("Highlights",5,true);
        Component link = new Component("Link",2,false);
        Component list = new Component( "List",6,true);
        Component footer = new Component("Footer",7,false);


        List<Component> components = new ArrayList<>();
        components.add(carousel);
        components.add(list);
        components.add(link);
        components.add(footer);


        return components;
    }
}
