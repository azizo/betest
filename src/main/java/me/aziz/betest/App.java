package me.aziz.betest;

import me.aziz.betest.services.ComponentSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class App {

    @Autowired
    private ComponentSettingService componentSettingService;

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void addInitData() {
        componentSettingService.saveSetting(componentSettingService.insertSampleData());

    }
}