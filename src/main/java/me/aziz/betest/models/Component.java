package me.aziz.betest.models;

import javax.persistence.*;
import java.util.Comparator;

@Entity
@Table(name = "components")
public class Component {
    @Id
    @GeneratedValue
    private int id;
    private String title;
    private int ranking;
    private boolean header;

    public Component() {
    }

    public Component(String title, int ranking, boolean header) {
        this.title = title;
        this.ranking = ranking;
        this.header = header;
    }


    public Component(int ranking, boolean header) {
        this.ranking = ranking;
        this.header = header;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}