package me.aziz.betest.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentRepo extends JpaRepository<Component, Integer> {
}