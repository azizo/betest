package me.aziz.betest.models;

import java.util.List;

public class ComponentDTO {

    private List<Component> components;

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }
}
